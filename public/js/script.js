
const cards = document.querySelectorAll('.card');

function transition() {
  if (this.classList.contains('act')) {
    this.classList.remove('act')
  } else {
    this.classList.add('act');
  }
}

cards.forEach(card => card.addEventListener('click', transition));
 