@extends('adminlte.principal')

@section('content')
<div class="row">
    <!-- right column -->
    <div class="col-xs-12">
        <div class="box box-solid box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Informacion de Areas de Soporte</h3>&nbsp;
                <a class="btn btn-danger  btn-md" href="{{ route('archivo') }}" >Archivo</a>
            </div>
            <link href="css/style.css" rel="stylesheet">
            <!-- /.box-header -->

            <div class="box-body">
                <!--Auditoria  -->
                <div class="col-xs-12 col-sm-6 col-md-3 ">
                    <div class="form-group">
                        <div class="cardContainer">
                            <div class="card">
                                <div class="side front">
                                    <div class="img img1"></div>
                                    <div class="info">
                                        <h2>Auditoría</h2>
                                        <div class="btn">
                                            <h4>Informacion</h4>
                                            <svg fill="#333" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M8.59 16.34l4.58-4.59-4.58-4.59L10 5.75l6 6-6 6z" />
                                                <path d="M0-.25h24v24H0z" fill="none" /></svg>
                                        </div>
                                    </div>
                                </div>
                                <div class="side back">
                                    <div class="info">
                                        <h2>
                                            <center>Auditoría</center>
                                        </h2>
                                        <ul>
                                            <li>Horario : Lun. a Vier 08:00 -17hs</li>
                                            <li>Email : auditoria@borigenbetzel.com.ar</li>
                                            <li>Telefono : 0387-4859632 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                0387-4896325
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Recursos Humanos  -->
                <div class="col-xs-12 col-sm-6 col-md-3 ">
                    <div class="form-group">
                        <div class="cardContainer inactive">
                            <div class="card">
                                <div class="side front">
                                    <div class="img img2"></div>
                                    <div class="info">
                                        <h2>Recursos Humanos</h2>
                                        <div class="btn">
                                            <h4>Informacion</h4>
                                            <svg fill="#333" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M8.59 16.34l4.58-4.59-4.58-4.59L10 5.75l6 6-6 6z" />
                                                <path d="M0-.25h24v24H0z" fill="none" /></svg>
                                        </div>
                                    </div>
                                </div>
                                <div class="side back">
                                    <div class="info">
                                        <h2>
                                            <center>Recursos Humanos</center>
                                        </h2>
                                        <ul>
                                            <li>Your choice of VX4 decks ranging from 60 to 72 inches</li>
                                            <li>The 37hp Vanguard BigBlock EFI makes short work out of big jobs enabling speeds up to 16mph</li>
                                            <li>Massive 24" drive tires and 13" front caster tires</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Sistemas  -->
                <div class="col-xs-12 col-sm-6 col-md-3 ">
                    <div class="form-group">
                        <div class="cardContainer inactive">
                            <div class="card">
                                <div class="side front">
                                    <div class="img img3"></div>
                                    <div class="info">
                                        <h2>Sistemas</h2>
                                        <div class="btn">
                                            <h4>Informacion</h4>
                                            <svg fill="#333" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M8.59 16.34l4.58-4.59-4.58-4.59L10 5.75l6 6-6 6z" />
                                                <path d="M0-.25h24v24H0z" fill="none" /></svg>
                                        </div>
                                    </div>
                                </div>
                                <div class="side back">
                                    <div class="info">
                                        <h2>
                                            <center>Sistemas</center>
                                        </h2>
                                        <ul>
                                            <li>V-Twin OHV Technology provides superior balance, low vibration, lower emissions, better fuel economy and
                                                higher HP/Weight</li>
                                            <li>Advanced Debris Management keeps engine clean and cool for enhanced durability and performance</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Servicio Tecnico  -->
                <div class="col-xs-12 col-sm-6 col-md-3 ">
                    <div class="form-group">
                        <div class="cardContainer inactive">
                            <div class="card">
                                <div class="side front">
                                    <div class="img img4"></div>
                                    <div class="info">
                                        <h2>Servicio Técnico</h2>
                                        <div class="btn">
                                            <h4>Informacion</h4>
                                            <svg fill="#333" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M8.59 16.34l4.58-4.59-4.58-4.59L10 5.75l6 6-6 6z" />
                                                <path d="M0-.25h24v24H0z" fill="none" /></svg>
                                        </div>
                                    </div>
                                </div>
                                <div class="side back">
                                    <div class="info">
                                        <h2>
                                            <center>Servicio Técnico</center>
                                        </h2>
                                        <ul>
                                            <li>V-Twin OHV Technology provides superior balance, low vibration, lower emissions, better fuel economy and
                                                higher HP/Weight</li>
                                            <li>Advanced Debris Management keeps engine clean and cool for enhanced durability and performance</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Administracion  -->
                <div class="col-xs-12 col-sm-6col-md-3  ">
                    <div class="form-group">
                        <div class="cardContainer inactive">
                            <div class="card">
                                <div class="side front">
                                    <div class="img img5"></div>
                                    <div class="info">
                                        <h2>
                                            Administración
                                        </h2>
                                        <div class="btn">
                                            <h4>Informacion</h4>
                                            <svg fill="#333" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M8.59 16.34l4.58-4.59-4.58-4.59L10 5.75l6 6-6 6z" />
                                                <path d="M0-.25h24v24H0z" fill="none" /></svg>
                                        </div>
                                    </div>
                                </div>
                                <div class="side back">
                                    <div class="info">
                                        <h2>
                                            <center>Administración</center>
                                        </h2>
                                        <ul>
                                            <li>V-Twin OHV Technology provides superior balance, low vibration, lower emissions, better fuel economy and
                                                higher HP/Weight</li>
                                            <li>Advanced Debris Management keeps engine clean and cool for enhanced durability and performance</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <script src="js/script.js"></script>
                        </div>
                    </div>
                </div>


            </div> <!-- /.box-body -->
        </div> <!-- /.box-->
    </div> <!-- /.col -->
</div> <!-- /.row -->
<script src="js/script.js"></script>
@endsection